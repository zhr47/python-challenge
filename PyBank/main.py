#import dependencies
import os
import csv

#define variables
months = []
profit_loss_changes = []

count_months = 0
net_profit_loss = 0
previous_month_profit_loss = 0
current_month_profit_loss = 0
profit_loss_change = 0

#path to csvfile
budget_data = os.path.join("Resources", "budget_data.csv")


#open and read csv
with open(budget_data, newline="") as csvfile:

    csv_reader = csv.reader(csvfile, delimiter=",")

    #read header
    csv_header = next(csvfile)
        
    #read each row
    for row in csv_reader:

        #count of months
        count_months += 1

        #total amount of "Profit/Losses" 
        current_month_profit_loss = int(row[1])
        net_profit_loss += current_month_profit_loss

        if (count_months == 1):
            # Make the value of previous month to be equal to current month
            previous_month_profit_loss = current_month_profit_loss
            continue

        else:
            #calculate changes
            profit_loss_change = current_month_profit_loss - previous_month_profit_loss
            months.append(row[0])
            profit_loss_changes.append(profit_loss_change)
            previous_month_profit_loss = current_month_profit_loss

    #sum and average of the changes in "Profit/Losses" 
    sum_profit_loss = sum(profit_loss_changes)
    average_profit_loss = round(sum_profit_loss/(count_months - 1), 2)

    #highest and lowest changes in "Profit/Losses" 
    greatest_change = max(profit_loss_changes)
    lowest_change = min(profit_loss_changes)

    #locate the index value of highest and lowest changes in "Profit/Losses" 
    highest_month_index = profit_loss_changes.index(greatest_change)
    lowest_month_index = profit_loss_changes.index(lowest_change)

    #assign best and worst month
    best_month = months[highest_month_index]
    worst_month = months[lowest_month_index]

#print the analysis
print("Financial Analysis")
print("----------------------------")
print(f"Total Months:  {count_months}")
print(f"Total:  ${net_profit_loss}")
print(f"Average Change:  ${average_profit_loss}")
print(f"Greatest Increase in Profits:  {best_month} (${greatest_change})")
print(f"Greatest Decrease in Losses:  {worst_month} (${lowest_change})")


#export budget data.txt
budget_summary = os.path.join('..', 'PyBank', 'Analysis', 'budget_summary.txt')
with open(budget_summary, "w", newline='') as outfile:
    write = csv.writer(outfile)
    outfile.write("Financial Analysis\n")
    outfile.write("----------------------------\n")
    outfile.write(f"Total Months:  {count_months}\n")
    outfile.write(f"Total:  ${net_profit_loss}\n")
    outfile.write(f"Average Change:  ${average_profit_loss}\n")
    outfile.write(f"Greatest Increase in Profits:  {best_month} (${greatest_change})\n")
    outfile.write(f"Greatest Decrease in Losses:  {worst_month} (${lowest_change})\n")